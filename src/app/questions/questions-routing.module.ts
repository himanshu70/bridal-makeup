import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuestionType } from '../model/question.type.model';
import { QuestionListComponent } from './question-list/question-list.component';
import { QuestionTypesComponent } from './question-types/question-types.component';

const routes: Routes = [
  {
    path:"list",
    component: QuestionListComponent
  },
  {
    path:"types",
    component: QuestionTypesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionsRoutingModule { }
