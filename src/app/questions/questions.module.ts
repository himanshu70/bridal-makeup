import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuestionsRoutingModule } from './questions-routing.module';
import { QuestionListComponent } from './question-list/question-list.component';
import { QuestionTypesComponent } from './question-types/question-types.component';
import { LightboxModule } from 'ngx-lightbox';
import { YouTubePlayerModule } from '@angular/youtube-player';

@NgModule({
  declarations: [QuestionListComponent, QuestionTypesComponent],
  imports: [
    CommonModule,
    QuestionsRoutingModule,
    LightboxModule,
    YouTubePlayerModule

  ]
})
export class QuestionsModule { }
