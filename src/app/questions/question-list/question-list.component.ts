declare var $:any;
import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, ElementRef, Inject, NgZone, OnInit, Renderer2, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NavigationEnd, Router } from '@angular/router';
import { Lightbox } from 'ngx-lightbox';
import { DataShareService } from 'src/app/core/service/data-share.service';
import { QuestionsService } from 'src/app/core/service/questions.service';
import { SubchapterService } from 'src/app/core/service/subchapter.service';
import { Question } from 'src/app/model/question.model';
import { SubChapter } from 'src/app/model/subchapter.model';
import { VideoAnswer } from 'src/app/model/video.answer.model';
import { map,filter,pairwise ,throttleTime} from 'rxjs/operators';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit,AfterViewInit {

  textAnswers=[];
  questionList:Question[]=[];
  subChapterList:SubChapter[]=[];
  subChapterIds:number[]=[];
  map:Map<any,any>;
  public isClicked = []; 
  array = [];
player:any;
  pageNumber:number=0;
  listener;

  busy=false;
  isLoading=true;
   noMoreData=false;
  thresholdLimit=false;

  pageSize:number=1;
  isShowLoading:boolean=false;
  constructor(private router: Router,
    private dataShareService:DataShareService,
    private questionService:QuestionsService,
    private subChapterService:SubchapterService,
    private _lightbox: Lightbox,
    private domSanitizer: DomSanitizer,
    private elRef: ElementRef,
    @Inject(DOCUMENT) private document: Document,
     private ngZone: NgZone,
     private renderer2: Renderer2
    ) {

      // this.listener = this.renderer2.listen('window', 'scroll', (e) => {
      //   console.log("sssssssssssss");
      // });
     }

  ngOnInit(): void {

    

    // if (this.document && this.document.body && this.document.body.querySelector && !this.document.body.querySelector('script[src="https://www.youtube.com/iframe_api"]') ){
    //   const tag = this.document.createElement('script');

    //   tag.src = "https://www.youtube.com/iframe_api";
    //   this.document.body.appendChild(tag);
    // }
    this.map=new Map(JSON.parse(this.dataShareService.getData("selectedData")));
     this.subChapterService.getAllSubChapters(this.map.get("chapterId")).subscribe( (response:any) => {
      this.subChapterList=response.body;
      for(let i=0;i < this.subChapterList.length;i++){
        this.subChapterIds.push(Number(this.subChapterList[i].id));
      }
      this.questionService.getQuestionsBySubChapter(this.subChapterIds,this.pageSize,this.pageNumber).subscribe( (response:any) => {
        this.questionList=response.body;  
        });
    });


 
   
    
    
  }

  onScrollDown() {
    console.log('scrolled down!!');
  }
 
  onScrollUp() {
    console.log('scrolled up!!');
  }


  subChapterChange(index:any,subChapter:SubChapter){
    console.log(subChapter)
    this.isClicked=[];
    this.isClicked[index] = (this.isClicked[index] ? false :true ); 
    let ids= [];
    ids.push(subChapter.id);
    this.subChapterIds=ids;
    this.pageNumber=0;
    this.questionList=[];
    this.questionService.getQuestionsBySubChapter(ids,this.pageSize,this.pageNumber).subscribe( (response:any) => {
      if(response.body != null && response.body != undefined && response.body.length !=0){ 
        this.questionList=response.body;
        this.busy = false;
        this.thresholdLimit=false;
        this.noMoreData = false; 
      }else{
        this.thresholdLimit=true;
        this.busy = false
        this.noMoreData = true;
      }
     
      });
  }

  //Video Player

  openVideo(videoAnser:VideoAnswer){

    console.log(videoAnser.videoUrl)
    $("#videoModal").modal('show');

    this.questionService.getVideoAnsById(videoAnser.id).subscribe((response:any) =>{
      console.log(response.body.description);
      this.player.loadVideoById(response.body.videoUrl);
    })
   
  }

  savePlayer(player) {
    this.player = player;
  }

  onPlayerReady(event) {
    console.log("ready event fired")
    event.target.playVideo();
  }

  pauseVideo() {
    this.player.pauseVideo();
  }

//Image Questions
open(questionDetail){
  let _albums=new Array({src: questionDetail.previewImageUrl,
    caption: questionDetail.description,
    thumb: "test"});
  this._lightbox.open(_albums,0);
}


  
 // Image Anser
  openText(questionDtlId,index){

    this.questionService.getImagAnsListDtlById(questionDtlId).subscribe((response:any) => {
      this._lightbox.open(response.body,0);
    });
  }

  
  
  

  ngAfterViewInit() {

    

    this.player = new YT.Player('yPlayer', {
      height: '390',
      width: '640',
      videoId: '',
      events: {
        'onReady': this.onPlayerReady
      }
    });

    var self=this;
    $(document).ready(function(){
      window.scrollTo(0, 0);

      $('#videoModal').on('hidden.bs.modal', function () {
        console.log("modal hide called ");
        self.player.videoUrl='';
        self.player.pauseVideo();
      });


     });

      window.onscroll = () => {
        let bottomOfWindow = document.documentElement.scrollTop + window.innerHeight >= document.documentElement.offsetHeight - (document.documentElement.offsetHeight*0.2);
        console.log("Scrolltop " + document.documentElement.scrollTop);            
        console.log("window.innerHeight " + window.innerHeight);     
        console.log("document.documentElement.offsetHeight " + document.documentElement.offsetHeight);  
        console.log("document.documentElement.offsetHeight*0.2 " + document.documentElement.offsetHeight*0.2);     
        console.log("bottomOfWindow" + bottomOfWindow);     
        console.log("busy" + this.busy);     
        console.log("this.thresholdLimit" + this.thresholdLimit);     
        if (bottomOfWindow && !this.busy && !this.thresholdLimit && this.subChapterIds != null && this.subChapterIds.length > 0) {
            
            this.busy = true;

            this.isShowLoading=true;
            this.questionService.getQuestionsBySubChapter(this.subChapterIds,this.pageSize,this.pageNumber).subscribe( (response:any) => {
              if(response.body != null && response.body != undefined && response.body.length !=0){ 
                console.log(" resopnse received "+response.body+" page number "+this.pageNumber);
                this.pageNumber =this.pageNumber + 1;
                this.questionList=this.questionList.concat(response.body);
                this.busy = false;
                this.thresholdLimit=false;
                this.noMoreData = false; 
              }else{
                console.log(" resopnse received empty"+" page number "+this.pageNumber)
                this.thresholdLimit=true;
                this.busy = false
                this.noMoreData = true;
              }
            });
          }
      }

     
}



}
