declare var $:any;
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChapterService } from 'src/app/core/service/chapter.service';
import { DataShareService } from 'src/app/core/service/data-share.service';
import { SubchapterService } from 'src/app/core/service/subchapter.service';
import { Chapter } from 'src/app/model/chapter.model';
import { Classes } from 'src/app/model/classes.model';
import { Institution } from 'src/app/model/institution.model';
import { Subject } from 'src/app/model/subject.model';

@Component({
  selector: 'app-question-types',
  templateUrl: './question-types.component.html',
  styleUrls: ['./question-types.component.scss']
})
export class QuestionTypesComponent implements OnInit {

  @ViewChild('scroll', { read: ElementRef }) public scroll: ElementRef<any>;
  
  selectedInstitution:Institution;
  selectedClasses:Classes;
  selectedSubject:Subject;
  selectedInstId:string;
  selectedClsId:string;
  selectedSubjId:string;

  chapterList:Chapter[]=[];
  flipClass:String[] = [];
  constructor(private router: Router,
    private cdRef: ChangeDetectorRef,
    private route: ActivatedRoute,
    
    private chapterService:ChapterService,
    private subChapterService:SubchapterService,
    private dataShareService:DataShareService) { }
    

  ngOnInit(): void {
    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.selectedInstId = params['instSelected'];
        this.selectedClsId=params['classSelected'];
        this.selectedSubjId=params['subjSelected'];

      });

      this.chapterService.getChaptersBySubjectId(this.selectedSubjId).subscribe( (response:any) => {
        this.chapterList=response.body;
       });
  }

  chapterCardClicked(chapter:Chapter){
   var map=new Map(JSON.parse(this.dataShareService.getData("selectedData")));
   if(map !=null ){
    map.set("chapterId",chapter.id);
    map.set("chapterName",chapter.name);
    this.dataShareService.addData("selectedData",JSON.stringify(Array.from(map.entries())));

   }else{
    console.log(" in questinotype map null ");
    console.log(this.dataShareService.getData("selectedData"));
   }
   
  
   this.router.navigate(['questions/list']);
  }
  


  divClicked(){
  
    $("#chapterSection").show();
    setTimeout(function(){
      document.getElementById("chapterSection").scrollIntoView({ behavior: 'smooth', block: 'center' })
     }, 550); 
   
   
   
  }

  changeStyle($event,index){
    console.log($event.type);
    this.flipClass[index] = $event.type == 'mouseenter' ? 'flipped' : '';
  }
  

  ngAfterViewInit() {

    $(document).ready(function(){
      // $('.flip').hover(function(){
      //  $(this).find('.card').toggleClass('flipped');
      // });
      $('#chapterSection').hide();
     });

    
    }


  
}
