import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookAppointmentComponent } from './component/book-appointment/book-appointment.component';
import { ChangePasswordComponent } from './component/change-password/change-password.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { SigninComponent } from './component/signin/signin.component';
import { Oauth2redirectHandlerComponent } from './core/service/oauth2redirect-handler/oauth2redirect-handler.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  { 
    path: 'dashboard',
    component: DashboardComponent ,children: [
      {path: 'login', component: SigninComponent}, 
      {path: 'reset-password', component: ChangePasswordComponent}
    ]
  },
  {
    path:"book-appointment",
    component: BookAppointmentComponent
  }
  // },
  // {
  //   path: 'questions',
  //   loadChildren: () => import('./questions/questions.module').then(m => m.QuestionsModule)
  // }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
