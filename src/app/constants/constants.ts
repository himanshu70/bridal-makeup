import { AppSettings } from 'src/app/global/appsetting.model';

export const API_BASE_URL = AppSettings.BASE_URL;
export const ACCESS_TOKEN = 'accessToken';

// below first was 4200 angular port where redirect take
export const OAUTH2_REDIRECT_URI = 'http://ec2-15-206-212-161.ap-south-1.compute.amazonaws.com:3030/oauth2/redirect'

export const GOOGLE_AUTH_URL = API_BASE_URL + 'oauth2/authorize/google?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const FACEBOOK_AUTH_URL = API_BASE_URL + '/oauth2/authorize/facebook?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const GITHUB_AUTH_URL = API_BASE_URL + '/oauth2/authorize/github?redirect_uri=' + OAUTH2_REDIRECT_URI;



export const GOOGLE_LOGIN = 'GOOGLE_LOGIN';
export const USER_REGISTER = 'USER_REGISTER';
export const USER_LOGIN = 'USER_LOGIN';

export const DIGITS_REG=/[^0-9]/g;
export const NAME_REG=/[^a-zA-Z ]*$/g;  // for name :alphabets and space
