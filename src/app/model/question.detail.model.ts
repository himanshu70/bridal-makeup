import { TextAnswer } from './text.answer.model';
import { VideoAnswer } from './video.answer.model';

export class QuestionDetails {
    id:string;
    session:string;
    textAnswers:TextAnswer[];
    videoAnswers:VideoAnswer[];
    year:string;
    imageUrl:string;
    previewImageUrl:string;
}