export class Institution{
    id:string;
    name:string;
    abbreviation:string;
    description:string;
    priority:string;
    classes:any[]=[];
   
}