import { Question } from './question.model';

export class TextAnswer{
    id:string;
    imageUrl:string;
    position:string;
    question:Question;
    previewImageUrl:string;
    uploadedFile:any;
    progress:number=0;
    description:string;
}