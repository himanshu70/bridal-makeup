import { Classes } from './classes.model';
import { Institution } from './institution.model';

export class Subject{
    id:string;
    name:string;
    code:string;
    description:string;
    priority:string;
    institution:Institution;
    classes:Classes;


}