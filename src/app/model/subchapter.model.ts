import { Chapter } from './chapter.model';
import { Subject } from './subject.model';

export class SubChapter {
    id:string;
    name:string;
    description:string;
    priority:string;
    active:boolean;
    subject:Subject;
    chapter:Chapter;
}