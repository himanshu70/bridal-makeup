import { SubChapter } from './subchapter.model';
import { Subject } from './subject.model';

export class Chapter {
    id:string;
    name:string;
    description:string;
    priority:string;
    isPremium:boolean;
    active:boolean;
    subject:Subject;
    subChapters:SubChapter[]=[];
}