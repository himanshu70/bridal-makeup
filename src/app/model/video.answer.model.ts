import { Question } from './question.model';

export class VideoAnswer{
    id:string;
    videoUrl:string;
    position:string;
    question:Question;
    description:string;

}