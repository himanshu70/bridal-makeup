import { Chapter } from './chapter.model';
import { Classes } from './classes.model';
import { Exam } from './exam.model';
import { Institution } from './institution.model';
import { QuestionDetails } from './question.detail.model';
import { QuestionType } from './question.type.model';
import { SubChapter } from './subchapter.model';
import { Subject } from './subject.model';

export class Question {
    id:string;
    questionDetail:QuestionDetails;
    active:boolean;
    exam:Exam;
    questionType:QuestionType;
    institution:Institution=new Institution();
    classes:Classes=new Classes();;
    difficultyLevel:number=0;
    chapter:Chapter=new Chapter();
    subject:Subject=new Subject();
    subChapter:SubChapter=new SubChapter();


    institutionList:Institution[]=[];
    classesList:Classes[]=[];
    chapterList:Chapter[]=[];
    subjectList:Subject[]=[];
    subChapterList:SubChapter[]=[];
    


}