declare var $:any;
import { AfterViewInit, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { GOOGLE_LOGIN, USER_LOGIN, USER_REGISTER } from 'src/app/constants/constants';
import { EventEmmitterService } from 'src/app/core/service/event-emmitter.service';
import { User } from 'src/app/core/model/user.model';
import { JwtService } from 'src/app/core/service/jwt.service';
import { UserService } from 'src/app/core/service/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit ,AfterViewInit,OnChanges,OnDestroy {
  isLoaded=false;
  isLoggedIn:boolean=false;

  GOOGLE_LOGIN=GOOGLE_LOGIN;

  defaultProfileImageUrl:string="../../../assets/header/user_profile.png";
  token:any;
  currentUser:User;
  constructor(private userService:UserService,  
    private jwtService: JwtService,
    private router: Router,
    private toastr: ToastrService,
    private eventEmmitter:EventEmmitterService,
    private cdRef: ChangeDetectorRef) {

     eventEmmitter.socialLoginQueue$.subscribe(
        authProvider => {
          if(authProvider === GOOGLE_LOGIN || authProvider === USER_REGISTER || authProvider === USER_LOGIN){
            this.isLoggedIn=true;
            this.currentUser=JSON.parse(this.userService.getCurrentUser());
            this.cdRef.detectChanges();
          }
          console.log("Event emitter social queue hit")
      });
      //after signup return true
     
     }

  bookAppointment(){
      this.router.navigate(['book-appointment']);
  }

  ngOnInit(): void {
    var use=this.userService.getCurrentUser();
    this.currentUser=use != undefined ? JSON.parse(use) : null;
    this.isLoggedIn=(this.jwtService.getToken() != null && this.jwtService.getToken() != undefined );
  
  }

  logout(){
    this.jwtService.destroyToken();
    this.userService.purgeUser();
    this.isLoggedIn=false;
    this.currentUser=null;
    this.cdRef.detectChanges();
    this.router.navigate(['dashboard']);
    this.toastr.error(" Logged Out Successfully ");
  }

  ngOnChanges() {
    // create header using child_id
    console.log("under header comonent"+this.isLoggedIn); 
  }

  collapse(){
    this.isLoaded= !this.isLoaded;
    var el=document.getElementById("signContent");
    if(this.isLoaded && el != null && el !=  undefined){
       el.style.display='none';
    }else if(el != null && el !=  undefined){
      setTimeout(function () {
        el.style.display='block'; 
    }, 200);
      
    }


  }

  ngAfterViewInit(){
    
 }

 ngOnDestroy() {
  console.log("header: destroy called")
}

}
