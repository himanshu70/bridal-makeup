declare var $:any;
import { AfterViewInit, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-book-appointment',
  templateUrl: './book-appointment.component.html',
  styleUrls: ['./book-appointment.component.scss']
})
export class BookAppointmentComponent implements OnInit,AfterViewInit {

  constructor() { }

  ngOnInit(): void {

   
  }

  ngAfterViewInit() {

    $(document).ready(function(){
      $("#dat").datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        //startDate: '-15d',
        //endDate: '+0d'
      });
  });

}


}
