declare var $:any;
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EventEmmitterService } from 'src/app/core/service/event-emmitter.service';
import { JwtService } from 'src/app/core/service/jwt.service';
import { UserService } from 'src/app/core/service/user.service';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  resetPasswordForm:any;
  changePwdForm:FormGroup;
  pwdMisMatch=false;
  confmPwd:any;
  isLoading=false;
  authToken:string;
  changePwdFlg:boolean=false;
  resetPwdFlg:boolean=false;
  constructor(private fb: FormBuilder,private userService:UserService,
    private jwtService: JwtService,
    private toastr: ToastrService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private eventEmmitter:EventEmmitterService,
    private route: ActivatedRoute) { 

      this.router.routeReuseStrategy.shouldReuseRoute = function () {
        return false;
      };
      
      this.router.events.subscribe((evt) => {
        if (evt instanceof NavigationEnd) {
          this.router.navigated = false;
          window.scrollTo(0, 0);
        }
      });

    }

  ngOnInit(): void {

    this.route
    .queryParams
    .subscribe(params => {
      // Defaults to 0 if no query param provided.
      this.authToken = params['token'];
      if(this.authToken === '' || this.authToken === null || this.authToken === undefined){
        if(this.jwtService.getToken() === null || this.jwtService.getToken() === '' || this.jwtService.getToken() === undefined){
          this.router.navigate(['/dashboard/login']);
        }else{
          this.changePwdFlg=true;
          this.applyFormValidation();
        }
      }else {
          this.userService.isValidToken(this.authToken).subscribe((response :any) =>{
            if(response.status == 200){
              this.resetPwdFlg=true;
              this.applyFormValidation();
            }
          } ,
            (error) => {
              if(error.status == 401 || error.status == 400){ 
                this.toastr.error(" Token expired. Pls Resent Email ");
                this.router.navigate(['/dashboard/login']);
              }
          });
        }   
    });
    this.changePwdForm = this.fb.group({
      oldPassword:["", Validators.required],
      newPassword: ["", Validators.required],
      confirmPassword: ["", Validators.required]
    });

    this.resetPasswordForm = this.fb.group({
      password: ["", Validators.required],
      confPassword: ["", Validators.required]
    });
  }

  resetPassword(){
    let pwd={newPassword: this.resetPasswordForm.get('password').value,token: this.authToken};
    this.isLoading=true;
    this.userService.resetPassword(pwd).subscribe((response:any) =>{
      if(response.status === 200){
        this.isLoading=false;
        this.toastr.success(" Password updated successfully ");
        $("#changePwdModal").modal('hide');
        this.router.navigate(['/dashboard/login']);
      }
    },
    (error) => {
        this.isLoading=false;
        this.toastr.error(" Error during password update");
    })
  }


  updatePassword(){
    this.isLoading=true;
    this.userService.updatedPassword(this.changePwdForm.value).subscribe((response:any) =>{
      if(response.status === 200){
        $("#changePwdModal").modal('hide');
        this.toastr.success(" Password updated successfully ");
        this.isLoading=false;
        this.router.navigate(['/dashboard/login']);
      }
    },
    (error) => {
        this.isLoading=false;
        this.toastr.error(" Error during password update");
    })
  }

  applyFormValidation(){
    var inputPwd=null;
    var pwd=null;
    if(this.resetPwdFlg){
      inputPwd=document.getElementById('confPassword');
       pwd= <HTMLInputElement>document.getElementById('password');
    }else{
      inputPwd=document.getElementById('confirmPassword');
       pwd= <HTMLInputElement>document.getElementById('newPassword');
    }
    
    this.confmPwd= fromEvent(inputPwd,'input').pipe( 
      map((i: any) => i.currentTarget.value),
      debounceTime(500)).subscribe(v=>{
          this.pwdMisMatch=pwd.value != '' && (v != pwd.value); 
          console.log(" vlaue of "+this.pwdMisMatch);
          this.cdRef.detectChanges();
        }
      );
  }
  ngAfterViewInit(){
    $(document).ready(function(){
      $("#changePwdModal").modal('show');
     
    });

     
  }

}
