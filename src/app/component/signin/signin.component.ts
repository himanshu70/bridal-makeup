declare var $: any;
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { JwtService } from 'src/app/core/service/jwt.service';
import { UserService } from 'src/app/core/service/user.service';

import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { GOOGLE_AUTH_URL, NAME_REG } from 'src/app/constants/constants';
import { EventEmmitterService } from 'src/app/core/service/event-emmitter.service';
import { USER_LOGIN, USER_REGISTER,DIGITS_REG } from 'src/app/constants/constants';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  loginForm: FormGroup;
  signUpForm: FormGroup;
  resetPwdForm: FormGroup;

  loggedUser: any;
  fieldTextType = false;
  pwdMisMatch = false;
  confmPwd: any;

  isRegister: boolean = false;
  isLogin: boolean = true;
  queryFlg: string;
  GOOGLE_AUTH_URL = GOOGLE_AUTH_URL;
  USER_REGISTER = USER_REGISTER;
  DIGITS_REG=DIGITS_REG;
  USER_LOGIN = USER_LOGIN;
  NAME_REG=NAME_REG;
  isLoading: boolean = false;
  showForgetPwd: boolean = false;
  constructor(private fb: FormBuilder, private userService: UserService,
    private jwtService: JwtService,
    private toastr: ToastrService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private route: ActivatedRoute,
    private elRef: ElementRef,
    private eventEmmitter: EventEmmitterService) {

    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        this.router.navigated = false;
        window.scrollTo(0, 0);
      }
    });


  }



  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }

  ngOnInit(): void {

    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.queryFlg = params['login'];

        if (this.queryFlg == 'false') {
          console.log("in rei" + this.queryFlg)
          this.isRegister = true;
          this.isLogin = false;
        } else {
          console.log("in lo" + this.queryFlg)
          this.isRegister = false;
          this.isLogin = true;
        }
      });

    this.signUpForm = this.fb.group({   
      email: ["", [Validators.required, Validators.email]],
      firstName: ["", [Validators.required]],
      lastName: [""],
      designation: ["-1", Validators.required],
      password: ["", Validators.required],
      confirmPassword: ["", Validators.required]
    });


    this.loginForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required]
    });

    this.resetPwdForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]]
    })
  }

  showForgotPwd() {
    this.showForgetPwd = true;
    this.isLogin = false;
    this.isRegister = false;
  }


  resetPassword() {
    console.log("resset passwrod ");
    this.isLoading = true;
    this.userService.forgotPassword(this.resetPwdForm.get('email').value).subscribe((response) => {
      console.log(response)
      if (response.status == 200) {
        this.isLoading = false;
        this.toastr.success(" Email sent successfully. Pls check email for password reset .");
        $("#loginModal").modal('hide');
        this.router.navigate(['/dashboard']);
      }
    },
      (error) => {
        if (error.status == 404 || error.status == 400) {
          this.toastr.error(" Email not registered Pls Sign up ");
          this.router.navigate(['/dashboard/login'], { queryParams: { login: false } });
        }
        this.isLoading = false;
      }
    );
  }

  validSignupForm() {
    var loginForm = this.loginForm;
    let errorFlg = false;
    if (this.signUpForm.get("email").value === null || this.signUpForm.get("email").value === undefined || this.signUpForm.get("email").value === '') {
      errorFlg = true;
      this.toastr.error(" Email can not be blank");
    } else if (this.signUpForm.get("firstName").value === null || this.signUpForm.get("firstName").value === undefined || this.signUpForm.get("firstName").value === '') {
      errorFlg = true;
      this.toastr.error(" First Name can not be blank");
    } else if (this.signUpForm.get("designation").value === null || this.signUpForm.get("designation").value === undefined || this.signUpForm.get("designation").value === '-1') {
      errorFlg = true;
      this.toastr.error(" Designation can not be blank");
    } else if (this.signUpForm.get("password").value === null || this.signUpForm.get("password").value === undefined || this.signUpForm.get("password").value === '') {
      errorFlg = true;
      this.toastr.error(" Password can not be blank");
    } else if (this.signUpForm.get("password").value != this.signUpForm.get("confirmPassword").value ) {
      errorFlg = true;
      this.toastr.error(" Password not matched");
    }
    return errorFlg;
  }

  submitSignupForm() {
    this.isLoading = true;
    if (!this.validSignupForm()) {
      this.userService.registerUser(this.signUpForm.value).subscribe((response) => {
        console.log(response)
        if (response.status == 200) {
          this.isLoading = false;
          this.jwtService.saveToken(response.headers.get('Authorization'));
          this.userService.saveUser(response.body);
          this.loggedUser = response.body;
          this.eventEmmitter.publishLoginEvent(USER_REGISTER);
          this.toastr.success("Account registered in successfully ");
          $("#loginModal").modal('hide');
          this.router.navigate(['/dashboard']);
        }
      },
        (error) => {
          if (error.status == 409) {
            this.isLoading = false;
            this.toastr.error(" Email already registered Pls Login ");
            this.router.navigate(['/dashboard/login']);
          } else {
            this.isLoading = false;
            this.toastr.error(" Error during account create ");
          }

        }
      )

    } else {
      this.isLoading = false;
    }

  }

  ceateAccount() {
    console.log("register clicked");
    this.isLogin = false;
    this.isRegister = true;
    this.showForgetPwd = false;

  }

  showLogin() {
    this.isLogin = true;
    this.isRegister = false;
    this.showForgetPwd = false;
  }



  isValidLogin(): boolean {
    var loginForm = this.loginForm;
    let errorFlg = false;
    if (this.loginForm.get("email").value == null || this.loginForm.get("email").value == undefined || this.loginForm.get("email").value == '') {
      errorFlg = true;
      this.loginForm.get("email").markAsDirty();
      this.elRef.nativeElement.querySelector('[formcontrolname="' + "email" + '"]').focus(); 
      this.toastr.error(" Email can not be blank");
    } else if (this.loginForm.get("password").value === null || this.loginForm.get("password").value === undefined || this.loginForm.get("password").value === '') {
      errorFlg = true;
      this.elRef.nativeElement.querySelector('[formcontrolname="' + "password" + '"]').focus(); 
      this.loginForm.get("password").markAsDirty();
      this.toastr.error(" Password can not be blank");

    }
    return errorFlg;
  }

  login() {
    this.isLoading = true;
    if (!this.isValidLogin()) {
      this.userService.login(this.loginForm.value)
        .subscribe((response) => {
          console.log(response);
          if (response.status == 200) {
            this.isLoading = false;
            this.jwtService.saveToken(response.headers.get('Authorization'));
            this.loggedUser = response.body;
            this.userService.saveUser(response.body);
            this.toastr.success("Logged in successfully ");
            $("#loginModal").modal('hide');
            this.eventEmmitter.publishLoginEvent(USER_LOGIN);
            this.router.navigate(['/dashboard'], { queryParams: { isLoggedIn: true } });
            this.cdRef.markForCheck();
          }
        },
          (error) => {
            if (error.status == 403) {
              this.toastr.error(" Permission not given to this credential ");
            } else if (error.status == 404) {
              this.toastr.error(" Not Found ! Pls check email ");
            } else if (error.status == 409) {
              this.toastr.error(" Unauthorized ! Pls Enter correct email and password ");
            } else {
              this.toastr.error(" Unable to login. Please check credential ");
            }
            this.isLoading = false;
          })
    } else {
      this.isLoading = false;
    }

  }

  loginkeyPress(event){
    if (event.keyCode === 13) {
      console.log("enter key pressed")
     this.login();
    }
  }

  signUpkeyPress(event){
    if (event.keyCode === 13) {
      console.log("enter key pressed")
     this.submitSignupForm();
    }
  }

  ngAfterViewInit() {
    $(document).ready(function () {

      $(".navbar-toggler collapsed").click(() => {
        $('#signContent').css("z-index", "-1");
      });
      $("#loginModal").modal('show', {backdrop: 'static', keyboard: false});  

    });

    if (this.isRegister) {
      var inputPwd = document.getElementById('confirmPassword');
      var pwd = <HTMLInputElement>document.getElementById('password');
      this.confmPwd = fromEvent(inputPwd, 'input').pipe(
        map((i: any) => i.currentTarget.value),
        debounceTime(500)).subscribe(v => {
          this.pwdMisMatch = pwd.value != '' && (v != pwd.value);
          console.log(" vlaue of " + this.pwdMisMatch);
          this.cdRef.detectChanges();
        }
        );
    }
  }



}
