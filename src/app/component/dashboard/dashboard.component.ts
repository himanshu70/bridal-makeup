declare var $:any;
//import * as $ from 'jquery';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';
import { AfterContentInit, AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MakeupServices } from 'src/app/mock/MakeupServices';
import { PriceList } from 'src/app/mock/PriceList';
import { TestimonialList } from 'src/app/mock/Testimonial';
import { GalleryList } from 'src/app/mock/GalleryList';
import { Lightbox } from 'ngx-lightbox';
import { CourseFeeList } from 'src/app/mock/CourseFeeList';
import { SliderService } from 'src/app/core/service/slider.service';
import { PriceService } from 'src/app/core/service/price.service';
import { CourseFeeService } from 'src/app/core/service/course-fee.service';
import { TestimonialService } from 'src/app/core/service/testimonial.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit,AfterViewInit,OnDestroy,AfterContentInit  {
  

  makeupServices=MakeupServices;
  flipClass:String[] = [];
  testimonialList=[];
  galleryList=GalleryList;
  courseFeeList=[];
  courseFeeClass:String[] = [];
  sliderList:any[]=[];
  priceList:any[]=[];
  @ViewChild('myApp', {static: false}) el:ElementRef;
  constructor(private _lightbox: Lightbox,
    private sliderService:SliderService,
    private priceService:PriceService,
    private courseFeeService:CourseFeeService,
    private testimonialService:TestimonialService,
    private cdref:ChangeDetectorRef ) { 
    this.courseFeeClass[1]='popular';
  }

  ngOnInit(): void {
    this.sliderService.getAllSliders().subscribe((response:any) =>{
      this.sliderList=response.body;
    },
    (error) =>{
      console.log(error);
    })
  }

  mouseEnter(index){
    console.log(index)
    let id='#makeup_service_'+index;
    console.log(id);
    //$(id).effect( "scale",{percent:80});
    $(id).css({
      transition: 'all .7s ease-in-out',
      transform:'scale(1.1)',
    });
 }


mouseLeave(index){
  let id='#makeup_service_'+index;
  console.log(id);
  //$(id).effect( "scale",{percent:80});
  $(id).css({
    transition: 'all 0.2s ease-in-out',
    transform:'scale(1.0)',
  });
}

open(gallery){
  let _albums=new Array({src: gallery.imageUrl,
    caption: gallery.title,
    thumb: ""});
  this._lightbox.open(_albums,0);
}


changeStyle($event,index){
  console.log($event.type);
  this.flipClass[index] = $event.type == 'mouseenter' ? 'flipped' : '';
}

changeFeeStyle($event,index){
  console.log($event.type);
  if(index === 1){
    this.courseFeeClass[index] = $event.type == 'mouseenter' ? 'feeShadow popular' : 'popular';
  }else{
    this.courseFeeClass[index] = $event.type == 'mouseenter' ? 'feeShadow' : '';
  }
  
}

ngAfterContentInit(): void {
  console.log(this.el);
  //$(this.el.nativeElement).owlCarousel();
}

  ngAfterViewInit() {
    $('#mobileAppp').owlCarousel({ 
      loop:true,
      margin:10,
      nav:false,
      autoplay: true,
      autoplayTimeout:2000,
      items:1
   
    });
       this.priceService.getAllPrices().subscribe((response:any) =>{
        this.priceList=response.body;
      },
      (error) =>{
        console.log(error);
      });

      this.courseFeeService.getAllCourseFeeList().subscribe((response:any) =>{
        this.courseFeeList=response.body;
      },
      (error) =>{
        console.log(error);
      })
      this.testimonialService.getAllTestimonials().subscribe((response:any) =>{
        this.testimonialList=response.body;
        this.cdref.detectChanges();
        $('#mobileAppp').trigger('destroy.owl.carousel'); 
        $('#mobileAppp').find('.owl-stage-outer').children().unwrap();
        $('#mobileAppp').removeClass("owl-center owl-loaded owl-text-select-on");

        $('#mobileAppp').owlCarousel({ 
          loop:true,
          margin:10,
          nav:false,
          autoplay: true,
          autoplayTimeout:2000,
          items:1
        });
        
      },
      (error) =>{
        console.log(error);
      })
      

  }

  ngOnDestroy() {
   
  }

}
