export interface User {
    email: string;
    token: string;
    username: string;
    bio: string;
    image: string;
    password:string;
    address:string;
    isEmailVerified:string;
    isPremium:string;
    imageUrl:string;
    name:string;
    id:string;
  }