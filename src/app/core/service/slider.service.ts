import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppSettings } from 'src/app/global/appsetting.model';

@Injectable({
  providedIn: 'root'
})
export class SliderService {

  BASE_URL=AppSettings.BASE_URL;

  constructor(private http: HttpClient) { 

  }

  getAllSliders(){
    return  this.http.get(this.BASE_URL+'slider/list', {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }


}
