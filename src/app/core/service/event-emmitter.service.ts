import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventEmmitterService {

  private socialLoginQueue = new Subject<string>();

  socialLoginQueue$ = this.socialLoginQueue.asObservable();

  publishLoginEvent(authProvider: string) {
    this.socialLoginQueue.next(authProvider);
  }


  constructor() { 


  }
}
