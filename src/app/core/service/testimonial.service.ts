import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppSettings } from 'src/app/global/appsetting.model';

@Injectable({
  providedIn: 'root'
})
export class TestimonialService {

  BASE_URL=AppSettings.BASE_URL;

  constructor(private http: HttpClient) { 

  }

  getAllTestimonials(){
    return  this.http.get(this.BASE_URL+'testimonial/list', {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }

}
