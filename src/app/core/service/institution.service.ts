import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppSettings } from 'src/app/global/appsetting.model';

@Injectable({
  providedIn: 'root'
})
export class InstitutionService {
  BASE_URL=AppSettings.BASE_URL;

  constructor(private http: HttpClient) { 

  }

  getAllIstitutions(){
    return  this.http.get(this.BASE_URL+'api/v1/institutions', {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }

  getInstitutionById(id){
    return  this.http.get(this.BASE_URL+'api/v1/institution/'+id, {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }

 


  /*************************************************************************** */
  getInstitutionsDropDown(){
    return  this.http.get(this.BASE_URL+'api/v1/institutions/dropdown?pageSize=10&pageNumber=0&sort=ASC&sortOn=priority', {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }

  getClassesByInstIdDropDown(id){
    return  this.http.get(this.BASE_URL+'api/v1/classes?institutionId='+id+'&pageSize=10&pageNumber=0&sort=ASC&sortOn=priority', {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }

}
