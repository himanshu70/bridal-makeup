import { Injectable } from '@angular/core';

@Injectable()
export class JwtService {

  constructor() { 
    
  }

  getToken(): string {
    return window.localStorage['authToken'];
  }

  saveToken(token: String) {
    window.localStorage['authToken'] = token;
  }

  destroyToken() {
    window.localStorage.removeItem('authToken');
  }
}
