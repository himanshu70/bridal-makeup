import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataShareService {

  addData(key:string, data:any){
    window.sessionStorage.setItem(key,data);
  }

  getData(key:string){
   return window.sessionStorage.getItem(key);
  }

  removeData(key:string){
    window.sessionStorage.removeItem(key);
  }

  clear(){
    window.sessionStorage.clear();
  }
}
