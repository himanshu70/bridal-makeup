import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppSettings } from 'src/app/global/appsetting.model';

@Injectable({
  providedIn: 'root'
})
export class ChapterService {
  BASE_URL=AppSettings.BASE_URL;

  constructor(private http: HttpClient) { 

  }

  getAllChapters(){
    return  this.http.get(this.BASE_URL+'api/v1/chapters?pageSize=10&pageNumber=0&sort=ASC&sortOn=priority', {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }

  getChapterById(id){
    return  this.http.get(this.BASE_URL+'api/v1/chapter/'+id, {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }

  

  getChaptersBySubjectId(subjectId){
    return  this.http.get(this.BASE_URL+'api/v1/chapters?subjectId='+subjectId+'&pageSize=10&pageNumber=0&sort=ASC&sortOn=priority', {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }
}
