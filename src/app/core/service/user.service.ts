import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtService } from './jwt.service';
import { logging } from 'protractor';
import { AppSettings } from 'src/app/global/appsetting.model';


@Injectable()
export class UserService {

   BASE_URL=AppSettings.BASE_URL;

  constructor(private http: HttpClient) { }

  

  getUserById(userId:any){
    return  this.http.get(this.BASE_URL+'users/'+userId, {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }
 

 login(user:any){
  return  this.http.post(this.BASE_URL+'users/login', user,{
    headers: new HttpHeaders({'Content-Type':  'application/json',
      'Accept':  'application/json'}),
    observe: 'response'
  })
 }

 registerUser(user:any){
  return  this.http.post(this.BASE_URL+'users/register', user,{
    headers: new HttpHeaders({'Content-Type':  'application/json',
      'Accept':  'application/json'}),    observe: 'response'
  })
 }

 socialLogin(socialUser:any){
  return  this.http.post(this.BASE_URL+'users/login/social', socialUser,{
    headers: new HttpHeaders({'Content-Type':  'application/json',
      'Accept':  'application/json'}),
    observe: 'response'
  })
 }

 isLoggedIn(authToken: string,userId:string){ 
  return  this.http.get(this.BASE_URL+'users/'+userId+'/isLoggedIn', {
    headers: new HttpHeaders({'Content-Type':  'application/json',
      'Accept':  'application/json'}),
    observe: 'response'
  })
 }
 

// User Storage
saveUser(user: any) {
  window.localStorage['user_details'] = JSON.stringify(user);
}

purgeUser() {
  window.localStorage.removeItem('user_details');
}

getCurrentUser(): string {
  return window.localStorage['user_details'];
}

forgotPassword(email:string){
  return  this.http.get(this.BASE_URL+'users/forgotpassword?email='+email, {
    headers: new HttpHeaders({'Content-Type':  'application/json',
      'Accept':  'application/json'}),
      observe: 'response'
  });
}

isValidToken(token :string){
  return  this.http.get(this.BASE_URL+'users/changePassword?token='+token, {
    headers: new HttpHeaders({'Content-Type':  'application/json',
      'Accept':  'application/json'}),
      observe: 'response'
  });
}

updatedPassword(pwd:any){
  return  this.http.post(this.BASE_URL+'users/updatePassword', pwd,{  
    headers: new HttpHeaders({'Content-Type':  'application/json',
      'Accept':  'application/json'}),    observe: 'response'
  })
}

resetPassword(pwd:any){
  return  this.http.post(this.BASE_URL+'users/resetPassword', pwd,{  
    headers: new HttpHeaders({'Content-Type':  'application/json',
      'Accept':  'application/json'}),    observe: 'response'
  })
}




}
