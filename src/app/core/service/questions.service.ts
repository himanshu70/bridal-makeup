import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AppSettings } from 'src/app/global/appsetting.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {
  BASE_URL=AppSettings.BASE_URL;

  constructor(private http: HttpClient,
    private domSanitizer: DomSanitizer) { 

  }
  getQuestionsBySubChapter(subchapterId:any,pageSize:any,pageNumber:any){
    return  this.http.get(this.BASE_URL+'api/v1/questions?subChapterIds='+subchapterId+'&pageSize='+pageSize+'&pageNumber='+pageNumber, {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }

  getVideo(){
    return  this.http.get(this.BASE_URL+'api/v1/video1', { responseType: 'blob'})
   

  }

  getVideo1(){
      return this.http
       .get(this.BASE_URL+'api/v1/video1', { responseType: 'blob'});
    
  }


  getVideo2(){
    return  this.http.get(this.BASE_URL+'api/v1/video2', { responseType: 'blob'});
  }

  getVideo23(){
    return  this.http.get(this.BASE_URL+'api/v1/video1/stream/mp4/toystory', { 
      headers: new HttpHeaders({'Content-Type':  'video/mp4',
      'Accept':  'video/mp4'}),
      responseType: 'blob'});  
  }




  /*****************Non QUestions ********************* */
  getImagAnsListDtlById(questionDtlId:any){
    return  this.http.get(this.BASE_URL+'api/v1/image/answers/'+questionDtlId, {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }

  
  getVideoAnsById(videoAnsId){
    return  this.http.get(this.BASE_URL+'api/v1/video/answers/'+videoAnsId, {
      headers: new HttpHeaders({'Content-Type':  'application/json',
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }
  
}
