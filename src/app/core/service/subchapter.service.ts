import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppSettings } from 'src/app/global/appsetting.model';

@Injectable({
  providedIn: 'root'
})
export class SubchapterService {
  BASE_URL=AppSettings.BASE_URL;

  constructor(private http: HttpClient) { 

  }

  getAllSubChapters(chaperId){
    return  this.http.get(this.BASE_URL+'api/v1/subchapters?chapterId='+chaperId+'&pageSize=10&pageNumber=0&sort=ASC&sortOn=priority', {
      headers: new HttpHeaders({'Content-Type':  'application/json', 
        'Accept':  'application/json'}),
      observe: 'response'
    })
  }
}
