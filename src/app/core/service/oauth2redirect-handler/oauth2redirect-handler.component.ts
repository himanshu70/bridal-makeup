import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EventEmmitterService } from '../event-emmitter.service';
import { JwtService } from '../jwt.service';
import { UserService } from '../user.service';
import { GOOGLE_LOGIN } from 'src/app/constants/constants';

@Component({
  selector: 'app-oauth2redirect-handler',
  templateUrl: './oauth2redirect-handler.component.html',
  styleUrls: ['./oauth2redirect-handler.component.scss']
})
export class Oauth2redirectHandlerComponent implements OnInit {

  GOOGLE_LOGIN=GOOGLE_LOGIN;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private jwtService: JwtService,
    private userService:UserService,
    private eventEmmitter:EventEmmitterService) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      const auth_token = params['auth_token'];
      if(auth_token) {
        this.jwtService.saveToken(auth_token);
        const userId = params['userId'];
        this.userService.getUserById(userId).subscribe(response =>{
          console.log(response)
          if(response.status == 200){
            this.userService.saveUser(response.body);
            this.eventEmmitter.publishLoginEvent(GOOGLE_LOGIN);
            this.router.navigate(['/dashboard']);
          }
        },
          (error) => {
           console.log(" errror "+error);
           
          }
        );
        this.router.navigate(['dashboard']);
    }else{
      console.log( " not auth token receive : "+auth_token);
    }
  });
}

  

}
