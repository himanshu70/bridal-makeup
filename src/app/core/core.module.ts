import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from './service/user.service';
import { AuthGuardService } from './service/auth-guard.service';
import { JwtService } from './service/jwt.service';
import { Oauth2redirectHandlerComponent } from './service/oauth2redirect-handler/oauth2redirect-handler.component';



@NgModule({
  declarations: [Oauth2redirectHandlerComponent],
  imports: [
    CommonModule
  ],
  providers: [UserService,AuthGuardService,JwtService],
})
export class CoreModule { 
  
}
