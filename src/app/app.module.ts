import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './component/footer/footer.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { HeaderComponent } from './component/header/header.component';
import { SigninComponent } from './component/signin/signin.component';
import { CoreModule } from './core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HttpTokenInterceptor } from './core/interceptors/http.token.interceptor';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { DataShareService } from './core/service/data-share.service';
import { ChangePasswordComponent } from './component/change-password/change-password.component';
import { KeyPressValidatorDirective } from './util/key-press-validator.directive';
import { LightboxModule } from 'ngx-lightbox';
import { BookAppointmentComponent } from './component/book-appointment/book-appointment.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    FooterComponent,
    HeaderComponent,
    SigninComponent,
    ChangePasswordComponent,
    KeyPressValidatorDirective,
    BookAppointmentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,  
    CoreModule,
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    LightboxModule
    
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true },DataShareService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
