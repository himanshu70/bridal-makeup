export const GalleryList=[

    {
        id:1,
        imageUrl:'../../../assets/makeup/Gallery/01.jpg',
        title:'Bridal Makeup'

    },
    
    {
        id:2,
        imageUrl:'../../../assets/makeup/Gallery/02.jpg',
        title:'Eye Makeup'

    },
    
    {
        id:3,
        imageUrl:'../../../assets/makeup/Gallery/03.jpg',
        title:'Face Makeup'

    },
    
    {
        id:4,
        imageUrl:'../../../assets/makeup/Gallery/04.jpg',
        title:'Eyebrow Makeup'

    },
    
    {
        id:5,
        imageUrl:'../../../assets/makeup/Gallery/05.jpg',
        title:'Haircut Makeup'

    },
    
    {
        id:6,
        imageUrl:'../../../assets/makeup/Gallery/06.jpg',
        title:'Facia Makeup'

    },
    {
        id:7,
        imageUrl:'../../../assets/makeup/Gallery/07.jpg',
        title:'Dressing Table'

    },
    {
        id:8,
        imageUrl:'../../../assets/makeup/Gallery/08.jpg',
        title:'Dressing Table'

    }
];