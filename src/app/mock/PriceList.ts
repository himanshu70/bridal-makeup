export const PriceList=[
{
    id:1,
    title:'Bridal Makeup',
    price:3000,
    imageUrl:'../../../assets/makeup/makeup-1289325_1920.jpg',
    features:[
        {
            id:1,
            description:' ABC 1 '
        },
        {
            id:2,
            description:' ABC 2 '
        },
        {
            id:3,
            description:' ABC 3 '
        }
    ],
    discount:{
        id:1,
        title:'Wedding Season Dhamaka',
        discount:'20%'
    }
},
{
    id:2,
    title:'Face Makeup',
    price:2000,
    imageUrl:'../../../assets/makeup/makeup-791303_1920.jpg',
    features:[
        {
            id:1,
            description:' ABC 1 '
        },
        {
            id:2,
            description:' ABC 2 '
        },
        {
            id:3,
            description:' ABC 3 '
        }
    ]
},
{
    id:3,
    title:'Hair Makeup',
    price:5000,
    imageUrl:'../../../assets/makeup/makeup-1209798_1920.jpg',
    features:[
        {
            id:1,
            description:' ABC 1 '
        },
        {
            id:2,
            description:' ABC 2 '
        },
        {
            id:3,
            description:' ABC 3 '
        }
    ],
    discount:{
        id:1,
        title:'Wedding Season Dhamaka',
        discount:'10%'
    }
},
{
    id:4,
    title:'Facial Makeup',
    price:7000,
    imageUrl:'../../../assets/makeup/makeup-1209798_1920.jpg',
    features:[
        {
            id:1,
            description:' ABC 1 '
        },
        {
            id:2,
            description:' ABC 2 '
        },
        {
            id:3,
            description:' ABC 3 '
        }
    ],
    discount:{
        id:1,
        title:'Wedding Season Dhamaka',
        discount:'15%'
    }
}


];