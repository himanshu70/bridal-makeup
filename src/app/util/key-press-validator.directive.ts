import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appKeyPressValidator]'
})
export class KeyPressValidatorDirective {
  @Input() pattern: any;

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }
  ngOnInit() {
    console.log("Pattern is "+this.pattern);
}


  @HostListener('keyup', ['$event']) onKeyPress(e) {
    console.log("Firing keupress directive "+this.pattern)
    var val = this.elementRef.nativeElement.value;
    console.log("value received  "+val);
    val = val.replace(new RegExp(this.pattern),'');
    this.elementRef.nativeElement.value='';
    this.elementRef.nativeElement.value=val;
    console.log("value processed  "+val);
    console.log("Final element value value processed  "+this.elementRef.nativeElement.value);
    console.log("Exiting keypress dir")
  
  }

}
